# CI/CD  Pipeline-project 
- Jenkins project using declarative pipeline with groovy.
- Simulating development process With simple nodeJs file containing HTML, CSS and Javascript. 
- Project contains test files and one main JenkinsFile.

- The Pipeline contains three 4 stages - Build, Test, Deploy, Clean.
- Creating docker and nodeJs environment using Jenkins.

- With my own DockerHub and Github credentials create repo in DockerHub and clone the repo from my github account.

- Build the docker image.

- Using relay webhook each changes is automatically updated in Github repository.

- With each building the version in DockerHub is updated.

- After each deploy-stage the workspace is cleaned.